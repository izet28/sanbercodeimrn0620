// soal no 1
const balikString =(a)=>{
    let c = a.length
    let hasil=[]
    for(i=c-1;i>=0;i--){
         hasil+=(a[i])
         
    }
    return hasil
    }
    
    console.log(balikString("abcde")) // edcba
    console.log(balikString("rusak")) // kasur
    console.log(balikString("racecar")) // racecar
    console.log(balikString("haji")) // ijah

// soal no 2
const palindrome =(a)=>{
        let c = a.length
        let hasil=[]
        for(i=c-1;i>=0;i--){
             hasil+=(a[i])
             }
        if (hasil == a){
            return true
        }else{
            return false
        }
        }

console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta"))

// soal no 3

const bandingkan =(a,b)=>{
    
    if (a<0 || b <0){
        return -1
    }
    if (a==b){
        return -1
    }
    if (a==0 && b==0){
        return -1
    }
    if (!a){
        return b
    }if(!b){
        return a
    }
    return (a > b ? a : b)

   
    
}

console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18

