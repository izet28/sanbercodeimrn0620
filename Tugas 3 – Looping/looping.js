// SOAL 1
console.log("SOAL 1")

var i = 1
console.log("LOOPING PERTAMA")
while (i < 21) {
    if (i % 2 === 0) {
        console.log(i + ' - I love coding')
    }
    i++
}
// sekarang i = 21
console.log("LOOPING KEDUA")
while (i > 0) {
    if (i % 2 === 0) {
        console.log(i + ' - I will become a mobile developer')
    }
    i--
}
console.log()




// SOAL 2
console.log("SOAL 2")
console.log("OUTPUT")
for (i = 1; i < 21; i++) {
    if ((i % 3 === 0) && (i % 2 !== 0)) {
        console.log(i + ' - I Love Coding')
    }
    else if (i % 2 !== 0) {
        console.log(i + ' - Santai')
    }
    else if (i % 2 === 0) {
        console.log(i + ' - Berkualitas')
    }
}
console.log()




// SOAL 3
var s=''

for (i=0;i<4;i++){
    for(j=0;j<=8;j++){
        s+='#'
    }
    
    s += '\n'
}
console.log(s)




// SOAL 4
var s=''

for (i=0;i<7;i++){
    for(j=0;j<=i;j++){
        s+='#'
    }
    
    s += '\n'
}
console.log(s)



// SOAL 5
var s=''
papan=8
for (i=0;i<papan;i++){
    for(j=0;j<papan;j++){
    
        if((i%2)==0){
            if((j%2)==0){
                s+=' '
            }
            else{
                s+='#'
            }
        }else{
            if((j%2)==0){
                s+='#'
            }
            else{
                s+=' '
            }
        }
      
        
    }
    
    s += '\n'
    
}
console.log(s+t)